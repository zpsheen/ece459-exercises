use std::thread;

static N: i32 = 20;  // number of threads to spawn

// You should modify main() to spawn multiple threads and join() them
fn main() {
    let mut children = vec![];

    for i in 0..N {
        // Need to have the move to move the variable children into the ownership of the closure
        let curr_handle = thread::spawn(move || {
            println!("In thread number {}", i);
        });
        children.push(curr_handle);
    }
    
    // to gain access again in the main, need to return the vector in the handler.join().unwrap()
    for child in children {
        child.join().unwrap();
    }
}
