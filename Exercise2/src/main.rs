// Recursive Method
// fn fibonacci_number(n: u32) -> u32 {
//     if n == 0 {
//         return 0
//     } else if n == 1 || n == 2 {
//         return 1
//     }
//     return fibonacci_number(n-1) + fibonacci_number(n-2)
// }

// Iterative with Memoization
fn fibonacci_number(n: u32) -> u32 {
    if n == 0 {
        return 0
    } else if n == 1 || n == 2 {
        return 1
    }
    // When iterating through a vec, the iterator needs to be of type usize
    let size:usize = n as usize;
    let mut fib = vec![1,1];

    // If we just used n here instead of size, big issues
    for i in 2..size {
        let curr_fib = fib[i-1] + fib[i-2];
        fib.push(curr_fib);
    }

    //println!("{:?}", fib);
    return fib[size-1]
}

fn main() {
    println!("{}", fibonacci_number(10));
}
