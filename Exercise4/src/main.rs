use std::sync::mpsc;
use std::thread;
use std::time::Duration;

static N:i32 = 10;

// You should modify main() to spawn threads and communicate using channels
fn main() {
    let (tx, rx) = mpsc::channel();

    // mpsc is multi-producer, single consumer FIFO
    let tx1 = mpsc::Sender::clone(&tx);
    let tx2 = mpsc::Sender::clone(&tx);
    let words = vec![
        String::from("Testing"), 
        String::from("in "), 
        String::from("thread"), 
        String::from("one!")
    ];

    thread::spawn(move || {
        for word in words {
            tx1.send(word).unwrap();
            thread::sleep(Duration::from_millis(1000));
        }
    });

    thread::spawn(move || {
        for _j in 0..N {
            let val = String::from("Second Thread");
            tx2.send(val).unwrap();
            thread::sleep(Duration::from_millis(1000));
        }
    });
    
    for received in rx {
        println!("Got: {}", received);
    }
}
